# How to process SusySkimHiggsino ntuples and save them as flat trees
Ntuples produced by the SusySkimAna framework steered by SusySkimHiggsino contain branches of every data type, from simple `int` to `std::vector` of generic objects. To process such ntuples and produce flat output trees that do not contain any vector branch and are suitable enough for machine learning studies, the analysis.C ROOT macro is used.

In the most recent version, the analysis code is steered via the `run_analysis.py` script which takes SusySkimHiggsino's ntuples as input and creates flat trees as ouput.To run the script you simply need to do
<pre>setupATLAS
lsetup root
python run_analysis.py -i input_path_to_the_ntuples -o output_path </pre>
where `-i` for the path to the input ntuples and `-o` for the path were the output ntuples are saved. If you want to process a sample with a specific name, you can add the flag `-n sample_name`. It's also possible to process just a subset of the input events by using `--MaxEvents number_of_events`. Finally, just in the case no `run_merge` was performed after producing the ntuples with SusySkimHiggsino, you can re-compute the `genWeight` on-the-fly by adding the flag `--doSumOfWeights`. 

# Run the analysis code on HTCondor system
Assuming you are running on the Milano cluster, you can parallelize the flat ntuple production by using the HTCondor system. For this purpose, two scripts are available under the `condor` folder:

1) A python script to create condor submission files
2) A python script to sequentially submit files to condor system

which create and submit .sub files to HTCondor for each input sample that has to be processed. What you need to do is to configure 
- the name of the input ntuples in the sample_list array of both python scripts
- the path to the working directory on the local machine where submit files are created, as well as the path to the local machine folder where you want to save the log files produced by the job
- the path to the input and output directories for the run_analysis.py script, and (if necessary) the name of the sample and the number of events to be processed
- some properties of the job like the quantity of memory that the system should allocate, the number of CPUs, ...

The same scripts should also work on `lxplus`, but be aware that some modifications are certainly needed. 
