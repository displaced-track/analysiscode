#ifndef analysis_h
#define analysis_h

#include <boost/algorithm/string.hpp>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TVector3.h"
#include "TLorentzVector.h"
#include "vector"

class analysis 
{

   public :

      TTree*          fChainNominal; // !pointer to the analyzed nominal/trk syst TTree or TChain 
      TTree*          fChainSyst;    // !pointer to the analyzed nominal/trk syst TTree or TChain 
      Int_t           fCurrent;      // !current Tree number in a TChain

      // Fixed size dimensions of array or collections stored in the TTree if any.

      // Declaration of leaf types of nominal tree
      Double_t        trigWeight_singleElectronTrig;
      Bool_t          trigMatch_singleElectronTrig;
      Double_t        trigWeight_singleMuonTrig;
      Bool_t          trigMatch_singleMuonTrig;
      Double_t        trigWeight_singlePhotonTrig;
      Bool_t          trigMatch_singlePhotonTrig;
      Double_t        trigWeight_metTrig;
      Bool_t          trigMatch_metTrig;
      Bool_t          IsMETTrigPassed;
      Int_t           year;
      Float_t         mu;
      Float_t         actual_mu;
      Int_t           nVtx;
      TVector3        *privtx;
      vector<TVector3> *pileupvtx;
      TVector3        *beampos;
      Int_t           nLep_base;
      Int_t           nMu_base;
      Int_t           nEle_base;
      Int_t           nPhoton_base;
      Int_t           nLep_signal;
      Int_t           nEle_signal;
      Int_t           nMu_signal;
      Int_t           nPhoton_signal;
      Int_t           lep1Flavor;
      Int_t           lep1Charge;
      Float_t         lep1Pt;
      Float_t         lep1Eta;
      Float_t         lep1Phi;
      Float_t         lep1M;
      Float_t         lep1D0;
      Float_t         lep1D0Sig;
      Float_t         lep1Z0;
      Float_t         lep1Z0SinTheta;
      Float_t         lep1MT_Met;
      Float_t         lep1_DPhiMet;
      Bool_t          lep1Baseline;
      Bool_t          lep1Signal;
      Int_t           lep1IFFType;
      Bool_t          lep1TruthMatched;
      Double_t        lep1Weight;
      Int_t           lep1Author;
      Int_t           lep1Type;
      Int_t           lep1Origin;
      Int_t           lep1EgMotherType;
      Int_t           lep1EgMotherOrigin;
      Int_t           lep1TruthCharge;
      Float_t         lep1TruthPt;
      Float_t         lep1TruthEta;
      Float_t         lep1TruthPhi;
      Float_t         lep1TruthM;
      Float_t         lep1TrackPt;
      Float_t         lep1TrackEta;
      Float_t         lep1TrackPhi;
      Int_t           lep2Flavor;
      Int_t           lep2Charge;
      Float_t         lep2Pt;
      Float_t         lep2Eta;
      Float_t         lep2Phi;
      Float_t         lep2M;
      Float_t         lep2D0;
      Float_t         lep2D0Sig;
      Float_t         lep2Z0;
      Float_t         lep2Z0SinTheta;
      Float_t         lep2MT_Met;
      Float_t         lep2_DPhiMet;
      Bool_t          lep2Baseline;
      Bool_t          lep2Signal;
      Int_t           lep2IFFType;
      Bool_t          lep2TruthMatched;
      Double_t        lep2Weight;
      Int_t           lep2Author;
      Int_t           lep2Type;
      Int_t           lep2Origin;
      Int_t           lep2EgMotherType;
      Int_t           lep2EgMotherOrigin;
      Int_t           lep2TruthCharge;
      Float_t         lep2TruthPt;
      Float_t         lep2TruthEta;
      Float_t         lep2TruthPhi;
      Float_t         lep2TruthM;
      Float_t         lep2TrackPt;
      Float_t         lep2TrackEta;
      Float_t         lep2TrackPhi;
      Int_t           nJet20;
      Int_t           nJet30;
      vector<float>   *jetPt;
      vector<float>   *jetEta;
      vector<float>   *jetPhi;
      vector<float>   *jetM;
      vector<bool>    *jetIsB;
      vector<int>     *jetTruthLabel;
      vector<int>     *jetNtrk;
      vector<float>   *jetTiming;
      vector<bool>    *jetPassTightBadWP;
      Int_t           nBJet20;
      Int_t           nBJet30;
      Float_t         met_Et;
      Float_t         met_Phi;
      Float_t         metTruth_Et;
      Float_t         metTruth_Phi;
      Float_t         met_track_Et;
      Float_t         met_track_Phi;
      Float_t         met_Signif;
      Float_t         metMuon_Et;
      Float_t         metMuon_Phi;
      Float_t         met_muons_invis_Et;
      Float_t         met_muons_invis_Phi;
      Float_t         met_muons_invis_Signif;
      Float_t         minDPhi_met_muons_invis_allJets20;
      Float_t         minDPhi_met_muons_invis_allJets30;
      Float_t         met_electrons_invis_Et;
      Float_t         met_electrons_invis_Phi;
      Float_t         met_electrons_invis_Signif;
      Float_t         minDPhi_met_electrons_invis_allJets20;
      Float_t         minDPhi_met_electrons_invis_allJets30;
      Float_t         met_photons_invis_Et;
      Float_t         met_photons_invis_Phi;
      Float_t         met_photons_invis_Signif;
      Float_t         minDPhi_met_photons_invis_allJets20;
      Float_t         minDPhi_met_photons_invis_allJets30;
      Float_t         met_leptons_invis_Et;
      Float_t         met_leptons_invis_Phi;
      Float_t         met_leptons_invis_Signif;
      Float_t         minDPhi_met_leptons_invis_allJets20;
      Float_t         minDPhi_met_leptons_invis_allJets30;
      Float_t         met_Et_LepInvis;
      Float_t         met_Phi_LepInvis;
      Float_t         met_Signif_LepInvis;
      vector<bool>    *trkisTight;
      vector<bool>    *trkisLoose;
      vector<float>   *trkPt;
      vector<float>   *trkEta;
      vector<float>   *trkPhi;
      vector<int>     *trkQ;
      vector<float>   *trkM;
      vector<float>   *trkZ0;
      vector<float>   *trkZ0Err;
      vector<float>   *trkZ0SinTheta;
      vector<float>   *trkD0;
      vector<float>   *trkD0Sig;
      vector<float>   *trkchiSquared;
      vector<float>   *trknumberDoF;
      vector<float>   *trkpixeldEdx;
      vector<float>   *trkTRTdEdx;
      vector<bool>    *trkisBaseMuon;
      vector<bool>    *trkisBaseElectron;
      vector<bool>    *trkisBasePhoton;
      vector<bool>    *trkisSignalMuon;
      vector<bool>    *trkisSignalElectron;
      vector<bool>    *trkisSignalPhoton;
      vector<int>     *trkLabel;
      vector<int>     *trkpdgId;
      vector<int>     *trknpar;
      vector<int>     *trkparbarcode;
      vector<int>     *trkparpdgId;
      vector<int>     *trkparnchild;
      vector<int>     *trkparchildpdgId;
      vector<float>   *trkparchildpt;
      vector<int>     *trkparchildbarcode;
      vector<int>     *trkBarcode;
      vector<int>     *trkStatus;
      vector<int>     *trkType;
      vector<int>     *trkOrigin;
      vector<bool>    *trkisSecTrk;
      vector<bool>    *trkMatchedToVSI;
      vector<TVector3> *trkVSIVtx;
      vector<int>     *trkVSIVtx_trkIndex;
      vector<float>   *trkVSIVtx_mass;
      vector<float>   *trkVSIVtx_chiSquared;
      vector<int>     *trkVSIVtx_numberDoF;
      vector<float>   *trkVSIVtx_minOpAng;
      vector<int>     *trkVSIVtx_num_trks;
      vector<float>   *trkVSIVtx_dCloseVrt;
      vector<int>     *trknIBLHits;
      vector<int>     *trknExpIBLHits;
      vector<int>     *trknExpBLayerHits;
      vector<int>     *trknBLayerHits;
      vector<int>     *trknPixHits;
      vector<int>     *trknPixLayers;
      vector<int>     *trknPixDeadSensors;
      vector<int>     *trknPixHoles;
      vector<int>     *trknPixSharedHits;
      vector<int>     *trknPixOutliers;
      vector<int>     *trknSCTHits;
      vector<int>     *trknSCTDeadSensors;
      vector<int>     *trknSCTHoles;
      vector<int>     *trknSCTSharedHits;
      vector<int>     *trknSCTOutliers;
      vector<float>   *trkPtcone20;
      vector<float>   *trkPtcone30;
      vector<float>   *trkPtcone40;
      vector<float>   *trkNonBaseAssocPtcone20;
      vector<float>   *trkNonBaseAssocPtcone30;
      vector<float>   *trkNonBaseAssocPtcone40;
      vector<float>   *trkNonBasePhotonConvPtcone20;
      vector<float>   *trkNonBasePhotonConvPtcone30;
      vector<float>   *trkNonBasePhotonConvPtcone40;
      vector<float>   *trkNonSignalAssocPtcone20;
      vector<float>   *trkNonSignalAssocPtcone30;
      vector<float>   *trkNonSignalAssocPtcone40;
      vector<float>   *trkNonSignalPhotonConvPtcone20;
      vector<float>   *trkNonSignalPhotonConvPtcone30;
      vector<float>   *trkNonSignalPhotonConvPtcone40;
      vector<float>   *trkBaseLepPhotonMinDR;
      vector<float>   *trkSignalLepPhotonMinDR;
      Float_t         minDPhi_met_allJets20;
      Float_t         minDPhi_met_allJets30;
      Bool_t          hasZCand;
      Float_t         mll;
      TLorentzVector  *Photon;
      Float_t         Photon_met_Et;
      Float_t         Photon_met_Phi;
      Float_t         Photon_minDPhi_met_allJets20;
      Float_t         Photon_minDPhi_met_allJets30;
      Int_t           Photon_conversionType;
      Float_t         Photon_conversionRadius;
      TString         *DecayProcess;
      Double_t        pileupWeight;
      Double_t        leptonWeight;
      Double_t        eventWeight;
      Double_t        genWeight;
      Double_t        bTagWeight;
      Double_t        trigWeight;
      Double_t        jvtWeight;
      Double_t        photonWeight;
      Double_t        genWeightUp;
      Double_t        genWeightDown;
      Float_t         x1;
      Float_t         x2;
      Float_t         pdf1;
      Float_t         pdf2;
      Float_t         scalePDF;
      Int_t           id1;
      Int_t           id2;
      ULong64_t       PRWHash;
      ULong64_t       EventNumber;
      Float_t         xsec;
      Float_t         GenHt;
      Float_t         GenMET;
      Int_t           DatasetNumber;
      Int_t           RunNumber;
      Int_t           RandomRunNumber;
      Int_t           FS;
      vector<float>   *LHE3Weights;
      vector<TString> *LHE3WeightNames;

      // List of branches
      TBranch        *b_trigWeight_singleElectronTrig;   //!
      TBranch        *b_trigMatch_singleElectronTrig;   //!
      TBranch        *b_trigWeight_singleMuonTrig;   //!
      TBranch        *b_trigMatch_singleMuonTrig;   //!
      TBranch        *b_trigWeight_singlePhotonTrig;   //!
      TBranch        *b_trigMatch_singlePhotonTrig;   //!
      TBranch        *b_trigWeight_metTrig;   //!
      TBranch        *b_trigMatch_metTrig;   //!
      TBranch        *b_IsMETTrigPassed;   //!
      TBranch        *b_year;   //!
      TBranch        *b_mu;   //!
      TBranch        *b_actual_mu;   //!
      TBranch        *b_nVtx;   //!
      TBranch        *b_privtx;   //!
      TBranch        *b_pileupvtx;   //!
      TBranch        *b_beampos;   //!
      TBranch        *b_nLep_base;   //!
      TBranch        *b_nMu_base;   //!
      TBranch        *b_nEle_base;   //!
      TBranch        *b_nPhoton_base;   //!
      TBranch        *b_nLep_signal;   //!
      TBranch        *b_nEle_signal;   //!
      TBranch        *b_nMu_signal;   //!
      TBranch        *b_nPhoton_signal;   //!
      TBranch        *b_lep1Flavor;   //!
      TBranch        *b_lep1Charge;   //!
      TBranch        *b_lep1Pt;   //!
      TBranch        *b_lep1Eta;   //!
      TBranch        *b_lep1Phi;   //!
      TBranch        *b_lep1M;   //!
      TBranch        *b_lep1D0;   //!
      TBranch        *b_lep1D0Sig;   //!
      TBranch        *b_lep1Z0;   //!
      TBranch        *b_lep1Z0SinTheta;   //!
      TBranch        *b_lep1MT_Met;   //!
      TBranch        *b_lep1_DPhiMet;   //!
      TBranch        *b_lep1Baseline;   //!
      TBranch        *b_lep1Signal;   //!
      TBranch        *b_lep1IFFType;   //!
      TBranch        *b_lep1TruthMatched;   //!
      TBranch        *b_lep1Weight;   //!
      TBranch        *b_lep1Author;   //!
      TBranch        *b_lep1Type;   //!
      TBranch        *b_lep1Origin;   //!
      TBranch        *b_lep1EgMotherType;   //!
      TBranch        *b_lep1EgMotherOrigin;   //!
      TBranch        *b_lep1TruthCharge;   //!
      TBranch        *b_lep1TruthPt;   //!
      TBranch        *b_lep1TruthEta;   //!
      TBranch        *b_lep1TruthPhi;   //!
      TBranch        *b_lep1TruthM;   //!
      TBranch        *b_lep1TrackPt;   //!
      TBranch        *b_lep1TrackEta;   //!
      TBranch        *b_lep1TrackPhi;   //!
      TBranch        *b_lep2Flavor;   //!
      TBranch        *b_lep2Charge;   //!
      TBranch        *b_lep2Pt;   //!
      TBranch        *b_lep2Eta;   //!
      TBranch        *b_lep2Phi;   //!
      TBranch        *b_lep2M;   //!
      TBranch        *b_lep2D0;   //!
      TBranch        *b_lep2D0Sig;   //!
      TBranch        *b_lep2Z0;   //!
      TBranch        *b_lep2Z0SinTheta;   //!
      TBranch        *b_lep2MT_Met;   //!
      TBranch        *b_lep2_DPhiMet;   //!
      TBranch        *b_lep2Baseline;   //!
      TBranch        *b_lep2Signal;   //!
      TBranch        *b_lep2IFFType;   //!
      TBranch        *b_lep2TruthMatched;   //!
      TBranch        *b_lep2Weight;   //!
      TBranch        *b_lep2Author;   //!
      TBranch        *b_lep2Type;   //!
      TBranch        *b_lep2Origin;   //!
      TBranch        *b_lep2EgMotherType;   //!
      TBranch        *b_lep2EgMotherOrigin;   //!
      TBranch        *b_lep2TruthCharge;   //!
      TBranch        *b_lep2TruthPt;   //!
      TBranch        *b_lep2TruthEta;   //!
      TBranch        *b_lep2TruthPhi;   //!
      TBranch        *b_lep2TruthM;   //!
      TBranch        *b_lep2TrackPt;   //!
      TBranch        *b_lep2TrackEta;   //!
      TBranch        *b_lep2TrackPhi;   //!
      TBranch        *b_nJet20;   //!
      TBranch        *b_nJet30;   //!
      TBranch        *b_jetPt;   //!
      TBranch        *b_jetEta;   //!
      TBranch        *b_jetPhi;   //!
      TBranch        *b_jetM;   //!
      TBranch        *b_jetIsB;   //!
      TBranch        *b_jetTruthLabel;   //!
      TBranch        *b_jetNtrk;   //!
      TBranch        *b_jetTiming;   //!
      TBranch        *b_jetPassTightBadWP;   //!
      TBranch        *b_nBJet20;   //!
      TBranch        *b_nBJet30;   //!
      TBranch        *b_met_Et;   //!
      TBranch        *b_met_Phi;   //!
      TBranch        *b_metTruth_Et;   //!
      TBranch        *b_metTruth_Phi;   //!
      TBranch        *b_met_track_Et;   //!
      TBranch        *b_met_track_Phi;   //!
      TBranch        *b_met_Signif;   //!
      TBranch        *b_metMuon_Et;   //!
      TBranch        *b_metMuon_Phi;   //!
      TBranch        *b_met_muons_invis_Et;   //!
      TBranch        *b_met_muons_invis_Phi;   //!
      TBranch        *b_met_muons_invis_Signif;   //!
      TBranch        *b_minDPhi_met_muons_invis_allJets20;   //!
      TBranch        *b_minDPhi_met_muons_invis_allJets30;   //!
      TBranch        *b_met_electrons_invis_Et;   //!
      TBranch        *b_met_electrons_invis_Phi;   //!
      TBranch        *b_met_electrons_invis_Signif;   //!
      TBranch        *b_minDPhi_met_electrons_invis_allJets20;   //!
      TBranch        *b_minDPhi_met_electrons_invis_allJets30;   //!
      TBranch        *b_met_photons_invis_Et;   //!
      TBranch        *b_met_photons_invis_Phi;   //!
      TBranch        *b_met_photons_invis_Signif;   //!
      TBranch        *b_minDPhi_met_photons_invis_allJets20;   //!
      TBranch        *b_minDPhi_met_photons_invis_allJets30;   //!
      TBranch        *b_met_leptons_invis_Et;   //!
      TBranch        *b_met_leptons_invis_Phi;   //!
      TBranch        *b_met_leptons_invis_Signif;   //!
      TBranch        *b_minDPhi_met_leptons_invis_allJets20;   //!
      TBranch        *b_minDPhi_met_leptons_invis_allJets30;   //!
      TBranch        *b_met_Et_LepInvis;   //!
      TBranch        *b_met_Phi_LepInvis;   //!
      TBranch        *b_met_Signif_LepInvis;   //!
      TBranch        *b_trkisTight;   //!
      TBranch        *b_trkisLoose;   //!
      TBranch        *b_trkPt;   //!
      TBranch        *b_trkEta;   //!
      TBranch        *b_trkPhi;   //!
      TBranch        *b_trkQ;   //!
      TBranch        *b_trkM;   //!
      TBranch        *b_trkZ0;   //!
      TBranch        *b_trkZ0Err;   //!
      TBranch        *b_trkZ0SinTheta;   //!
      TBranch        *b_trkD0;   //!
      TBranch        *b_trkD0Sig;   //!
      TBranch        *b_trkchiSquared;   //!
      TBranch        *b_trknumberDoF;   //!
      TBranch        *b_trkpixeldEdx;   //!
      TBranch        *b_trkTRTdEdx;   //!
      TBranch        *b_trkisBaseMuon;   //!
      TBranch        *b_trkisBaseElectron;   //!
      TBranch        *b_trkisBasePhoton;   //!
      TBranch        *b_trkisSignalMuon;   //!
      TBranch        *b_trkisSignalElectron;   //!
      TBranch        *b_trkisSignalPhoton;   //!
      TBranch        *b_trkLabel;   //!
      TBranch        *b_trkpdgId;   //!
      TBranch        *b_trknpar;   //!
      TBranch        *b_trkparbarcode;   //!
      TBranch        *b_trkparpdgId;   //!
      TBranch        *b_trkparnchild;   //!
      TBranch        *b_trkparchildpdgId;   //!
      TBranch        *b_trkparchildpt;   //!
      TBranch        *b_trkparchildbarcode;   //!
      TBranch        *b_trkBarcode;   //!
      TBranch        *b_trkStatus;   //!
      TBranch        *b_trkType;   //!
      TBranch        *b_trkOrigin;   //!
      TBranch        *b_trkisSecTrk;   //!
      TBranch        *b_trkMatchedToVSI;   //!
      TBranch        *b_trkVSIVtx;   //!
      TBranch        *b_trkVSIVtx_trkIndex;   //!
      TBranch        *b_trkVSIVtx_mass;   //!
      TBranch        *b_trkVSIVtx_chiSquared;   //!
      TBranch        *b_trkVSIVtx_numberDoF;   //!
      TBranch        *b_trkVSIVtx_minOpAng;   //!
      TBranch        *b_trkVSIVtx_num_trks;   //!
      TBranch        *b_trkVSIVtx_dCloseVrt;   //!
      TBranch        *b_trknIBLHits;   //!
      TBranch        *b_trknExpIBLHits;   //!
      TBranch        *b_trknExpBLayerHits;   //!
      TBranch        *b_trknBLayerHits;   //!
      TBranch        *b_trknPixHits;   //!
      TBranch        *b_trknPixLayers;   //!
      TBranch        *b_trknPixDeadSensors;   //!
      TBranch        *b_trknPixHoles;   //!
      TBranch        *b_trknPixSharedHits;   //!
      TBranch        *b_trknPixOutliers;   //!
      TBranch        *b_trknSCTHits;   //!
      TBranch        *b_trknSCTDeadSensors;   //!
      TBranch        *b_trknSCTHoles;   //!
      TBranch        *b_trknSCTSharedHits;   //!
      TBranch        *b_trknSCTOutliers;   //!
      TBranch        *b_trkPtcone20;   //!
      TBranch        *b_trkPtcone30;   //!
      TBranch        *b_trkPtcone40;   //!
      TBranch        *b_trkNonBaseAssocPtcone20;   //!
      TBranch        *b_trkNonBaseAssocPtcone30;   //!
      TBranch        *b_trkNonBaseAssocPtcone40;   //!
      TBranch        *b_trkNonBasePhotonConvPtcone20;   //!
      TBranch        *b_trkNonBasePhotonConvPtcone30;   //!
      TBranch        *b_trkNonBasePhotonConvPtcone40;   //!
      TBranch        *b_trkNonSignalAssocPtcone20;   //!
      TBranch        *b_trkNonSignalAssocPtcone30;   //!
      TBranch        *b_trkNonSignalAssocPtcone40;   //!
      TBranch        *b_trkNonSignalPhotonConvPtcone20;   //!
      TBranch        *b_trkNonSignalPhotonConvPtcone30;   //!
      TBranch        *b_trkNonSignalPhotonConvPtcone40;   //!
      TBranch        *b_trkBaseLepPhotonMinDR;   //!
      TBranch        *b_trkSignalLepPhotonMinDR;   //!
      TBranch        *b_minDPhi_met_allJets20;   //!
      TBranch        *b_minDPhi_met_allJets30;   //!
      TBranch        *b_hasZCand;   //!
      TBranch        *b_mll;   //!
      TBranch        *b_Photon;   //!
      TBranch        *b_Photon_met_Et;   //!
      TBranch        *b_Photon_met_Phi;   //!
      TBranch        *b_Photon_minDPhi_met_allJets20;   //!
      TBranch        *b_Photon_minDPhi_met_allJets30;   //!
      TBranch        *b_Photon_conversionType;   //!
      TBranch        *b_Photon_conversionRadius;   //!
      TBranch        *b_DecayProcess;   //!
      TBranch        *b_pileupWeight;   //!
      TBranch        *b_leptonWeight;   //!
      TBranch        *b_eventWeight;   //!
      TBranch        *b_genWeight;   //!
      TBranch        *b_bTagWeight;   //!
      TBranch        *b_trigWeight;   //!
      TBranch        *b_jvtWeight;   //!
      TBranch        *b_photonWeight;   //!
      TBranch        *b_genWeightUp;   //!
      TBranch        *b_genWeightDown;   //!
      TBranch        *b_x1;   //!
      TBranch        *b_x2;   //!
      TBranch        *b_pdf1;   //!
      TBranch        *b_pdf2;   //!
      TBranch        *b_scalePDF;   //!
      TBranch        *b_id1;   //!
      TBranch        *b_id2;   //!
      TBranch        *b_PRWHash;   //!
      TBranch        *b_EventNumber;   //!
      TBranch        *b_xsec;   //!
      TBranch        *b_GenHt;   //!
      TBranch        *b_GenMET;   //!
      TBranch        *b_DatasetNumber;   //!
      TBranch        *b_RunNumber;   //!
      TBranch        *b_RandomRunNumber;   //!
      TBranch        *b_FS;   //!
      TBranch        *b_LHE3Weights;   //!
      TBranch        *b_LHE3WeightNames;   //!

      // Class methods and data members
      analysis(TTree* tree = 0);
      virtual ~analysis();
      
      virtual Int_t    Cut(Long64_t entry);
      virtual Int_t    GetEntry(TTree* tree, Long64_t entry);
      virtual Long64_t LoadTree(TTree* tree, Long64_t entry);
      virtual Bool_t   Notify();
      virtual void     Show(TTree* tree, Long64_t entry = -1);
      
      virtual void     Init(TTree *tree);
      virtual void     LoopOnNominal(TTree* tree, Int_t nevents);
      virtual void     LoopOnSyst(TTree* tree, Int_t nevents);
      virtual void     LoopAndWrite(TTree* tree, const char* OutFileName, Int_t nEvents);
      
      Float_t          GetgenWeight();
      virtual void     GetMemUsage(double& vm_usage, double& resident_set);
      virtual void     GetSumOfWeights(TTree *CBKTree);
      virtual void     SaveLHE3Weights(std::vector<float> *LHE3Weights, std::vector<TString> *LHE3WeightNames, std::string tree_name, Int_t dsid);
      virtual void     FillHistogram(TH1D* h, Double_t entry, Double_t weight);   

      // Strings used to handle name exceptions 
      // when processing specific samples
      std::string tree_name;
      std::vector<std::string> tree_name_strs;  

      // Output tree pointer
      TTree* output_tree;

      // Output tree variables
      Float_t MU;    
      Int_t   EVENTNumber;                      
      Int_t   RUNnumber; 
      Int_t   DSID;                        
      Float_t GenWeight;                     
      Float_t EventWeight;                   
      Float_t LeptonWeight;                  
      Float_t PhotonWeight;                 
      Float_t JVTWeight;                 
      Float_t PileupWeight;                  
      Int_t   TrigMatch_met;                 
      Int_t   TrigMatch_Mu;                  
      Int_t   TrigMatch_Ele;  
      Int_t   TrigMatch_Gamma;               
      Float_t TrigWeight;                    
      Float_t TrigWeight_Mu;                 
      Float_t TrigWeight_Ele;                
      Float_t TrigWeight_Gamma;              
      Int_t   nLepBase;                      
      Int_t   nLepSig;                     
      Int_t   nMuBase;                       
      Int_t   nMuSig;                        
      Int_t   nEleBase;                     
      Int_t   nEleSig;                       
      Int_t   nPhotonBase;                   
      Int_t   nPhotonSignal;                 
      Int_t   HasZCand;                     
      Float_t Mll;                          
      Float_t mT;
      Int_t   Lep1Flavor;                   
      Int_t   Lep1Q;                         
      Float_t Lep1Pt;                       
      Float_t Lep1Eta;                       
      Float_t Lep1Phi;                       
      Int_t   Lep2Flavor;                    
      Int_t   Lep2Q;                       
      Float_t Lep2Pt;                        
      Float_t Lep2Eta;                      
      Float_t Lep2Phi;                       
      Int_t   nJet;                          
      Float_t LeadJetPt;                     
      Float_t LeadJetEta;                   
      Float_t LeadJetPhi;   
      Float_t LeadJetM;                 
      Float_t MET;                          
      Float_t MET_electrons;                 
      Float_t MET_muons;                     
      Float_t MET_photons;                   
      Float_t MET_leptons;                   
      Float_t MET_Phi;                       
      Float_t MET_electrons_Phi;             
      Float_t MET_muons_Phi;                 
      Float_t MET_photons_Phi;               
      Float_t MET_leptons_Phi;               
      Float_t MET_Signif;                    
      Float_t MET_electrons_Signif;          
      Float_t MET_muons_Signif;              
      Float_t MET_photons_Signif;            
      Float_t MET_leptons_Signif;            
      Float_t MET_dphi;                      
      Float_t MET_electrons_dphi;            
      Float_t MET_muons_dphi;                
      Float_t MET_photons_dphi;              
      Float_t MET_leptons_dphi;   
      Int_t   nTrack;          
      Float_t TrackPt;                       
      Float_t TrackEta;                     
      Float_t TrackPhi;                      
      Float_t TrackZ0;                       
      Float_t TrackD0;                       
      Float_t TrackD0Sig;                    
      Float_t TrackZ0SinTheta;              
      Int_t   TrackCharge;  
      Float_t TrackMt;               
      Float_t TrackFitQuality;              
      Int_t   TrackOrigin;                  
      Int_t   TrackType;                     
      Int_t   TrackLabel;                    
      Int_t   TrackPdgId;                    
      Int_t   TrackParPdgId;                 
      Int_t   TrackNIBLHits;                 
      Int_t   TrackPixHits;  
      Float_t TrackPixeldEdx;
      Float_t TrackTRTdEdx;                
      Int_t   TrackIsBaseMuon;              
      Int_t   TrackIsBaseElectron;           
      Int_t   TrackIsBasePhoton;             
      Int_t   TrackIsSignalMuon;             
      Int_t   TrackIsSignalElectron;         
      Int_t   TrackIsSignalPhoton;           
      Int_t   TrackIsSecTrk;                
      Int_t   TrackMatchedToVSI;             
      Float_t TrkNonBaseAssocPtcone20;       
      Float_t TrkNonBaseAssocPtcone30;       
      Float_t TrkNonBaseAssocPtcone40;       
      Float_t TrkNonBasePhotonConvPtcone20;  
      Float_t TrkNonBasePhotonConvPtcone30;  
      Float_t TrkNonBasePhotonConvPtcone40; 
      Float_t TrkNonSignalAssocPtcone20;    
      Float_t TrkNonSignalAssocPtcone30;     
      Float_t TrkNonSignalAssocPtcone40;     
      Float_t TrkNonSignalPhotonConvPtcone20;
      Float_t TrkNonSignalPhotonConvPtcone30;
      Float_t TrkNonSignalPhotonConvPtcone40;

   private:

      // Map storing CBK sumOfWeights: < <DSID, RunNumber>, SOWs >
      std::map< std::tuple<UInt_t, UInt_t>, Float_t > m_cbkSOWMap;

};
#endif

#ifdef analysis_cxx
analysis::analysis(TTree *tree) : fChainNominal(0)
{

   Init(tree);

}

analysis::~analysis()
{

   if (!fChainNominal || !fChainSyst) return;
   tree_name_strs.clear();

}

Int_t analysis::GetEntry(TTree* tree, Long64_t entry)
{

   // Read contents of entry.
   if (!tree) return 0;
   return tree->GetEntry(entry);

}

Long64_t analysis::LoadTree(TTree* tree, Long64_t entry)
{

   // Set the environment to read one entry
   if (!tree) return -5;
   Long64_t centry = tree->LoadTree(entry);
   if (centry < 0) return centry;
   if (tree->GetTreeNumber() != fCurrent) {
      fCurrent = tree->GetTreeNumber();
      Notify();
   }
   return centry;

}

void analysis::Init(TTree *tree)
{

   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   
   if (!tree) return;

   tree_name = tree->GetName();

   // Set branch addresses and branch pointers for nominal (and data) tree
   if (tree_name.find("data1") != std::string::npos || tree_name.find("data2") != std::string::npos || tree_name.find("NoSys") != std::string::npos){
    
      privtx = 0;
      pileupvtx = 0;
      beampos = 0;
      jetPt = 0;
      jetEta = 0;
      jetPhi = 0;
      jetM = 0;
      jetIsB = 0;
      jetTruthLabel = 0;
      jetNtrk = 0;
      jetTiming = 0;
      jetPassTightBadWP = 0;
      trkisTight = 0;
      trkisLoose = 0;
      trkPt = 0;
      trkEta = 0;
      trkPhi = 0;
      trkQ = 0;
      trkM = 0;
      trkZ0 = 0;
      trkZ0Err = 0;
      trkZ0SinTheta = 0;
      trkD0 = 0;
      trkD0Sig = 0;
      trkchiSquared = 0;
      trknumberDoF = 0;
      trkpixeldEdx = 0;
      trkTRTdEdx = 0;
      trkisBaseMuon = 0;
      trkisBaseElectron = 0;
      trkisBasePhoton = 0;
      trkisSignalMuon = 0;
      trkisSignalElectron = 0;
      trkisSignalPhoton = 0;
      trkLabel = 0;
      trkpdgId = 0;
      trknpar = 0;
      trkparbarcode = 0;
      trkparpdgId = 0;
      trkparnchild = 0;
      trkparchildpdgId = 0;
      trkparchildpt = 0;
      trkparchildbarcode = 0;
      trkBarcode = 0;
      trkStatus = 0;
      trkType = 0;
      trkOrigin = 0;
      trkisSecTrk = 0;
      trkMatchedToVSI = 0;
      trkVSIVtx = 0;
      trkVSIVtx_trkIndex = 0;
      trkVSIVtx_mass = 0;
      trkVSIVtx_chiSquared = 0;
      trkVSIVtx_numberDoF = 0;
      trkVSIVtx_minOpAng = 0;
      trkVSIVtx_num_trks = 0;
      trkVSIVtx_dCloseVrt = 0;
      trknIBLHits = 0;
      trknExpIBLHits = 0;
      trknExpBLayerHits = 0;
      trknBLayerHits = 0;
      trknPixHits = 0;
      trknPixLayers = 0;
      trknPixDeadSensors = 0;
      trknPixHoles = 0;
      trknPixSharedHits = 0;
      trknPixOutliers = 0;
      trknSCTHits = 0;
      trknSCTDeadSensors = 0;
      trknSCTHoles = 0;
      trknSCTSharedHits = 0;
      trknSCTOutliers = 0;
      trkPtcone20 = 0;
      trkPtcone30 = 0;
      trkPtcone40 = 0;
      trkNonBaseAssocPtcone20 = 0;
      trkNonBaseAssocPtcone30 = 0;
      trkNonBaseAssocPtcone40 = 0;
      trkNonBasePhotonConvPtcone20 = 0;
      trkNonBasePhotonConvPtcone30 = 0;
      trkNonBasePhotonConvPtcone40 = 0;
      trkNonSignalAssocPtcone20 = 0;
      trkNonSignalAssocPtcone30 = 0;
      trkNonSignalAssocPtcone40 = 0;
      trkNonSignalPhotonConvPtcone20 = 0;
      trkNonSignalPhotonConvPtcone30 = 0;
      trkNonSignalPhotonConvPtcone40 = 0;
      trkBaseLepPhotonMinDR = 0;
      trkSignalLepPhotonMinDR = 0;
      Photon = 0;
      DecayProcess = 0;
      LHE3Weights = 0;
      LHE3WeightNames = 0;
   
      fChainNominal = tree;
      fCurrent = -1;
      fChainNominal->SetMakeClass(1);

      // Set branch addresses and branch pointers
      fChainNominal->SetBranchAddress("trigWeight_singleElectronTrig", &trigWeight_singleElectronTrig, &b_trigWeight_singleElectronTrig);
      fChainNominal->SetBranchAddress("trigMatch_singleElectronTrig", &trigMatch_singleElectronTrig, &b_trigMatch_singleElectronTrig);
      fChainNominal->SetBranchAddress("trigWeight_singleMuonTrig", &trigWeight_singleMuonTrig, &b_trigWeight_singleMuonTrig);
      fChainNominal->SetBranchAddress("trigMatch_singleMuonTrig", &trigMatch_singleMuonTrig, &b_trigMatch_singleMuonTrig);
      fChainNominal->SetBranchAddress("trigWeight_singlePhotonTrig", &trigWeight_singlePhotonTrig, &b_trigWeight_singlePhotonTrig);
      fChainNominal->SetBranchAddress("trigMatch_singlePhotonTrig", &trigMatch_singlePhotonTrig, &b_trigMatch_singlePhotonTrig);
      fChainNominal->SetBranchAddress("trigWeight_metTrig", &trigWeight_metTrig, &b_trigWeight_metTrig);
      fChainNominal->SetBranchAddress("trigMatch_metTrig", &trigMatch_metTrig, &b_trigMatch_metTrig);
      fChainNominal->SetBranchAddress("IsMETTrigPassed", &IsMETTrigPassed, &b_IsMETTrigPassed);
      fChainNominal->SetBranchAddress("year", &year, &b_year);
      fChainNominal->SetBranchAddress("mu", &mu, &b_mu);
      fChainNominal->SetBranchAddress("actual_mu", &actual_mu, &b_actual_mu);
      fChainNominal->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
      fChainNominal->SetBranchAddress("privtx", &privtx, &b_privtx);
      fChainNominal->SetBranchAddress("pileupvtx", &pileupvtx, &b_pileupvtx);
      fChainNominal->SetBranchAddress("beampos", &beampos, &b_beampos);
      fChainNominal->SetBranchAddress("nLep_base", &nLep_base, &b_nLep_base);
      fChainNominal->SetBranchAddress("nMu_base", &nMu_base, &b_nMu_base);
      fChainNominal->SetBranchAddress("nEle_base", &nEle_base, &b_nEle_base);
      fChainNominal->SetBranchAddress("nPhoton_base", &nPhoton_base, &b_nPhoton_base);
      fChainNominal->SetBranchAddress("nLep_signal", &nLep_signal, &b_nLep_signal);
      fChainNominal->SetBranchAddress("nEle_signal", &nEle_signal, &b_nEle_signal);
      fChainNominal->SetBranchAddress("nMu_signal", &nMu_signal, &b_nMu_signal);
      fChainNominal->SetBranchAddress("nPhoton_signal", &nPhoton_signal, &b_nPhoton_signal);
      fChainNominal->SetBranchAddress("lep1Flavor", &lep1Flavor, &b_lep1Flavor);
      fChainNominal->SetBranchAddress("lep1Charge", &lep1Charge, &b_lep1Charge);
      fChainNominal->SetBranchAddress("lep1Pt", &lep1Pt, &b_lep1Pt);
      fChainNominal->SetBranchAddress("lep1Eta", &lep1Eta, &b_lep1Eta);
      fChainNominal->SetBranchAddress("lep1Phi", &lep1Phi, &b_lep1Phi);
      fChainNominal->SetBranchAddress("lep1M", &lep1M, &b_lep1M);
      fChainNominal->SetBranchAddress("lep1D0", &lep1D0, &b_lep1D0);
      fChainNominal->SetBranchAddress("lep1D0Sig", &lep1D0Sig, &b_lep1D0Sig);
      fChainNominal->SetBranchAddress("lep1Z0", &lep1Z0, &b_lep1Z0);
      fChainNominal->SetBranchAddress("lep1Z0SinTheta", &lep1Z0SinTheta, &b_lep1Z0SinTheta);
      fChainNominal->SetBranchAddress("lep1MT_Met", &lep1MT_Met, &b_lep1MT_Met);
      fChainNominal->SetBranchAddress("lep1_DPhiMet", &lep1_DPhiMet, &b_lep1_DPhiMet);
      fChainNominal->SetBranchAddress("lep1Baseline", &lep1Baseline, &b_lep1Baseline);
      fChainNominal->SetBranchAddress("lep1Signal", &lep1Signal, &b_lep1Signal);
      fChainNominal->SetBranchAddress("lep1IFFType", &lep1IFFType, &b_lep1IFFType);
      fChainNominal->SetBranchAddress("lep1TruthMatched", &lep1TruthMatched, &b_lep1TruthMatched);
      fChainNominal->SetBranchAddress("lep1Weight", &lep1Weight, &b_lep1Weight);
      fChainNominal->SetBranchAddress("lep1Author", &lep1Author, &b_lep1Author);
      fChainNominal->SetBranchAddress("lep1Type", &lep1Type, &b_lep1Type);
      fChainNominal->SetBranchAddress("lep1Origin", &lep1Origin, &b_lep1Origin);
      fChainNominal->SetBranchAddress("lep1EgMotherType", &lep1EgMotherType, &b_lep1EgMotherType);
      fChainNominal->SetBranchAddress("lep1EgMotherOrigin", &lep1EgMotherOrigin, &b_lep1EgMotherOrigin);
      fChainNominal->SetBranchAddress("lep1TruthCharge", &lep1TruthCharge, &b_lep1TruthCharge);
      fChainNominal->SetBranchAddress("lep1TruthPt", &lep1TruthPt, &b_lep1TruthPt);
      fChainNominal->SetBranchAddress("lep1TruthEta", &lep1TruthEta, &b_lep1TruthEta);
      fChainNominal->SetBranchAddress("lep1TruthPhi", &lep1TruthPhi, &b_lep1TruthPhi);
      fChainNominal->SetBranchAddress("lep1TruthM", &lep1TruthM, &b_lep1TruthM);
      fChainNominal->SetBranchAddress("lep1TrackPt", &lep1TrackPt, &b_lep1TrackPt);
      fChainNominal->SetBranchAddress("lep1TrackEta", &lep1TrackEta, &b_lep1TrackEta);
      fChainNominal->SetBranchAddress("lep1TrackPhi", &lep1TrackPhi, &b_lep1TrackPhi);
      fChainNominal->SetBranchAddress("lep2Flavor", &lep2Flavor, &b_lep2Flavor);
      fChainNominal->SetBranchAddress("lep2Charge", &lep2Charge, &b_lep2Charge);
      fChainNominal->SetBranchAddress("lep2Pt", &lep2Pt, &b_lep2Pt);
      fChainNominal->SetBranchAddress("lep2Eta", &lep2Eta, &b_lep2Eta);
      fChainNominal->SetBranchAddress("lep2Phi", &lep2Phi, &b_lep2Phi);
      fChainNominal->SetBranchAddress("lep2M", &lep2M, &b_lep2M);
      fChainNominal->SetBranchAddress("lep2D0", &lep2D0, &b_lep2D0);
      fChainNominal->SetBranchAddress("lep2D0Sig", &lep2D0Sig, &b_lep2D0Sig);
      fChainNominal->SetBranchAddress("lep2Z0", &lep2Z0, &b_lep2Z0);
      fChainNominal->SetBranchAddress("lep2Z0SinTheta", &lep2Z0SinTheta, &b_lep2Z0SinTheta);
      fChainNominal->SetBranchAddress("lep2MT_Met", &lep2MT_Met, &b_lep2MT_Met);
      fChainNominal->SetBranchAddress("lep2_DPhiMet", &lep2_DPhiMet, &b_lep2_DPhiMet);
      fChainNominal->SetBranchAddress("lep2Baseline", &lep2Baseline, &b_lep2Baseline);
      fChainNominal->SetBranchAddress("lep2Signal", &lep2Signal, &b_lep2Signal);
      fChainNominal->SetBranchAddress("lep2IFFType", &lep2IFFType, &b_lep2IFFType);
      fChainNominal->SetBranchAddress("lep2TruthMatched", &lep2TruthMatched, &b_lep2TruthMatched);
      fChainNominal->SetBranchAddress("lep2Weight", &lep2Weight, &b_lep2Weight);
      fChainNominal->SetBranchAddress("lep2Author", &lep2Author, &b_lep2Author);
      fChainNominal->SetBranchAddress("lep2Type", &lep2Type, &b_lep2Type);
      fChainNominal->SetBranchAddress("lep2Origin", &lep2Origin, &b_lep2Origin);
      fChainNominal->SetBranchAddress("lep2EgMotherType", &lep2EgMotherType, &b_lep2EgMotherType);
      fChainNominal->SetBranchAddress("lep2EgMotherOrigin", &lep2EgMotherOrigin, &b_lep2EgMotherOrigin);
      fChainNominal->SetBranchAddress("lep2TruthCharge", &lep2TruthCharge, &b_lep2TruthCharge);
      fChainNominal->SetBranchAddress("lep2TruthPt", &lep2TruthPt, &b_lep2TruthPt);
      fChainNominal->SetBranchAddress("lep2TruthEta", &lep2TruthEta, &b_lep2TruthEta);
      fChainNominal->SetBranchAddress("lep2TruthPhi", &lep2TruthPhi, &b_lep2TruthPhi);
      fChainNominal->SetBranchAddress("lep2TruthM", &lep2TruthM, &b_lep2TruthM);
      fChainNominal->SetBranchAddress("lep2TrackPt", &lep2TrackPt, &b_lep2TrackPt);
      fChainNominal->SetBranchAddress("lep2TrackEta", &lep2TrackEta, &b_lep2TrackEta);
      fChainNominal->SetBranchAddress("lep2TrackPhi", &lep2TrackPhi, &b_lep2TrackPhi);
      fChainNominal->SetBranchAddress("nJet20", &nJet20, &b_nJet20);
      fChainNominal->SetBranchAddress("nJet30", &nJet30, &b_nJet30);
      fChainNominal->SetBranchAddress("jetPt", &jetPt, &b_jetPt);
      fChainNominal->SetBranchAddress("jetEta", &jetEta, &b_jetEta);
      fChainNominal->SetBranchAddress("jetPhi", &jetPhi, &b_jetPhi);
      fChainNominal->SetBranchAddress("jetM", &jetM, &b_jetM);
      fChainNominal->SetBranchAddress("jetIsB", &jetIsB, &b_jetIsB);
      fChainNominal->SetBranchAddress("jetTruthLabel", &jetTruthLabel, &b_jetTruthLabel);
      fChainNominal->SetBranchAddress("jetNtrk", &jetNtrk, &b_jetNtrk);
      fChainNominal->SetBranchAddress("jetTiming", &jetTiming, &b_jetTiming);
      fChainNominal->SetBranchAddress("jetPassTightBadWP", &jetPassTightBadWP, &b_jetPassTightBadWP);
      fChainNominal->SetBranchAddress("nBJet20", &nBJet20, &b_nBJet20);
      fChainNominal->SetBranchAddress("nBJet30", &nBJet30, &b_nBJet30);
      fChainNominal->SetBranchAddress("met_Et", &met_Et, &b_met_Et);
      fChainNominal->SetBranchAddress("met_Phi", &met_Phi, &b_met_Phi);
      fChainNominal->SetBranchAddress("metTruth_Et", &metTruth_Et, &b_metTruth_Et);
      fChainNominal->SetBranchAddress("metTruth_Phi", &metTruth_Phi, &b_metTruth_Phi);
      fChainNominal->SetBranchAddress("met_track_Et", &met_track_Et, &b_met_track_Et);
      fChainNominal->SetBranchAddress("met_track_Phi", &met_track_Phi, &b_met_track_Phi);
      fChainNominal->SetBranchAddress("met_Signif", &met_Signif, &b_met_Signif);
      fChainNominal->SetBranchAddress("metMuon_Et", &metMuon_Et, &b_metMuon_Et);
      fChainNominal->SetBranchAddress("metMuon_Phi", &metMuon_Phi, &b_metMuon_Phi);
      fChainNominal->SetBranchAddress("met_muons_invis_Et", &met_muons_invis_Et, &b_met_muons_invis_Et);
      fChainNominal->SetBranchAddress("met_muons_invis_Phi", &met_muons_invis_Phi, &b_met_muons_invis_Phi);
      fChainNominal->SetBranchAddress("met_muons_invis_Signif", &met_muons_invis_Signif, &b_met_muons_invis_Signif);
      fChainNominal->SetBranchAddress("minDPhi_met_muons_invis_allJets20", &minDPhi_met_muons_invis_allJets20, &b_minDPhi_met_muons_invis_allJets20);
      fChainNominal->SetBranchAddress("minDPhi_met_muons_invis_allJets30", &minDPhi_met_muons_invis_allJets30, &b_minDPhi_met_muons_invis_allJets30);
      fChainNominal->SetBranchAddress("met_electrons_invis_Et", &met_electrons_invis_Et, &b_met_electrons_invis_Et);
      fChainNominal->SetBranchAddress("met_electrons_invis_Phi", &met_electrons_invis_Phi, &b_met_electrons_invis_Phi);
      fChainNominal->SetBranchAddress("met_electrons_invis_Signif", &met_electrons_invis_Signif, &b_met_electrons_invis_Signif);
      fChainNominal->SetBranchAddress("minDPhi_met_electrons_invis_allJets20", &minDPhi_met_electrons_invis_allJets20, &b_minDPhi_met_electrons_invis_allJets20);
      fChainNominal->SetBranchAddress("minDPhi_met_electrons_invis_allJets30", &minDPhi_met_electrons_invis_allJets30, &b_minDPhi_met_electrons_invis_allJets30);
      fChainNominal->SetBranchAddress("met_photons_invis_Et", &met_photons_invis_Et, &b_met_photons_invis_Et);
      fChainNominal->SetBranchAddress("met_photons_invis_Phi", &met_photons_invis_Phi, &b_met_photons_invis_Phi);
      fChainNominal->SetBranchAddress("met_photons_invis_Signif", &met_photons_invis_Signif, &b_met_photons_invis_Signif);
      fChainNominal->SetBranchAddress("minDPhi_met_photons_invis_allJets20", &minDPhi_met_photons_invis_allJets20, &b_minDPhi_met_photons_invis_allJets20);
      fChainNominal->SetBranchAddress("minDPhi_met_photons_invis_allJets30", &minDPhi_met_photons_invis_allJets30, &b_minDPhi_met_photons_invis_allJets30);
      fChainNominal->SetBranchAddress("met_leptons_invis_Et", &met_leptons_invis_Et, &b_met_leptons_invis_Et);
      fChainNominal->SetBranchAddress("met_leptons_invis_Phi", &met_leptons_invis_Phi, &b_met_leptons_invis_Phi);
      fChainNominal->SetBranchAddress("met_leptons_invis_Signif", &met_leptons_invis_Signif, &b_met_leptons_invis_Signif);
      fChainNominal->SetBranchAddress("minDPhi_met_leptons_invis_allJets20", &minDPhi_met_leptons_invis_allJets20, &b_minDPhi_met_leptons_invis_allJets20);
      fChainNominal->SetBranchAddress("minDPhi_met_leptons_invis_allJets30", &minDPhi_met_leptons_invis_allJets30, &b_minDPhi_met_leptons_invis_allJets30);
      fChainNominal->SetBranchAddress("met_Et_LepInvis", &met_Et_LepInvis, &b_met_Et_LepInvis);
      fChainNominal->SetBranchAddress("met_Phi_LepInvis", &met_Phi_LepInvis, &b_met_Phi_LepInvis);
      fChainNominal->SetBranchAddress("met_Signif_LepInvis", &met_Signif_LepInvis, &b_met_Signif_LepInvis);
      fChainNominal->SetBranchAddress("trkisTight", &trkisTight, &b_trkisTight);
      fChainNominal->SetBranchAddress("trkisLoose", &trkisLoose, &b_trkisLoose);
      fChainNominal->SetBranchAddress("trkPt", &trkPt, &b_trkPt);
      fChainNominal->SetBranchAddress("trkEta", &trkEta, &b_trkEta);
      fChainNominal->SetBranchAddress("trkPhi", &trkPhi, &b_trkPhi);
      fChainNominal->SetBranchAddress("trkQ", &trkQ, &b_trkQ);
      fChainNominal->SetBranchAddress("trkM", &trkM, &b_trkM);
      fChainNominal->SetBranchAddress("trkZ0", &trkZ0, &b_trkZ0);
      fChainNominal->SetBranchAddress("trkZ0Err", &trkZ0Err, &b_trkZ0Err);
      fChainNominal->SetBranchAddress("trkZ0SinTheta", &trkZ0SinTheta, &b_trkZ0SinTheta);
      fChainNominal->SetBranchAddress("trkD0", &trkD0, &b_trkD0);
      fChainNominal->SetBranchAddress("trkD0Sig", &trkD0Sig, &b_trkD0Sig);
      fChainNominal->SetBranchAddress("trkchiSquared", &trkchiSquared, &b_trkchiSquared);
      fChainNominal->SetBranchAddress("trknumberDoF", &trknumberDoF, &b_trknumberDoF);
      fChainNominal->SetBranchAddress("trkpixeldEdx", &trkpixeldEdx, &b_trkpixeldEdx);
      fChainNominal->SetBranchAddress("trkTRTdEdx", &trkTRTdEdx, &b_trkTRTdEdx);
      fChainNominal->SetBranchAddress("trkisBaseMuon", &trkisBaseMuon, &b_trkisBaseMuon);
      fChainNominal->SetBranchAddress("trkisBaseElectron", &trkisBaseElectron, &b_trkisBaseElectron);
      fChainNominal->SetBranchAddress("trkisBasePhoton", &trkisBasePhoton, &b_trkisBasePhoton);
      fChainNominal->SetBranchAddress("trkisSignalMuon", &trkisSignalMuon, &b_trkisSignalMuon);
      fChainNominal->SetBranchAddress("trkisSignalElectron", &trkisSignalElectron, &b_trkisSignalElectron);
      fChainNominal->SetBranchAddress("trkisSignalPhoton", &trkisSignalPhoton, &b_trkisSignalPhoton);
      fChainNominal->SetBranchAddress("trkLabel", &trkLabel, &b_trkLabel);
      fChainNominal->SetBranchAddress("trkpdgId", &trkpdgId, &b_trkpdgId);
      fChainNominal->SetBranchAddress("trknpar", &trknpar, &b_trknpar);
      fChainNominal->SetBranchAddress("trkparbarcode", &trkparbarcode, &b_trkparbarcode);
      fChainNominal->SetBranchAddress("trkparpdgId", &trkparpdgId, &b_trkparpdgId);
      fChainNominal->SetBranchAddress("trkparnchild", &trkparnchild, &b_trkparnchild);
      fChainNominal->SetBranchAddress("trkparchildpdgId", &trkparchildpdgId, &b_trkparchildpdgId);
      fChainNominal->SetBranchAddress("trkparchildpt", &trkparchildpt, &b_trkparchildpt);
      fChainNominal->SetBranchAddress("trkparchildbarcode", &trkparchildbarcode, &b_trkparchildbarcode);
      fChainNominal->SetBranchAddress("trkBarcode", &trkBarcode, &b_trkBarcode);
      fChainNominal->SetBranchAddress("trkStatus", &trkStatus, &b_trkStatus);
      fChainNominal->SetBranchAddress("trkType", &trkType, &b_trkType);
      fChainNominal->SetBranchAddress("trkOrigin", &trkOrigin, &b_trkOrigin);
      fChainNominal->SetBranchAddress("trkisSecTrk", &trkisSecTrk, &b_trkisSecTrk);
      fChainNominal->SetBranchAddress("trkMatchedToVSI", &trkMatchedToVSI, &b_trkMatchedToVSI);
      fChainNominal->SetBranchAddress("trkVSIVtx", &trkVSIVtx, &b_trkVSIVtx);
      fChainNominal->SetBranchAddress("trkVSIVtx_trkIndex", &trkVSIVtx_trkIndex, &b_trkVSIVtx_trkIndex);
      fChainNominal->SetBranchAddress("trkVSIVtx_mass", &trkVSIVtx_mass, &b_trkVSIVtx_mass);
      fChainNominal->SetBranchAddress("trkVSIVtx_chiSquared", &trkVSIVtx_chiSquared, &b_trkVSIVtx_chiSquared);
      fChainNominal->SetBranchAddress("trkVSIVtx_numberDoF", &trkVSIVtx_numberDoF, &b_trkVSIVtx_numberDoF);
      fChainNominal->SetBranchAddress("trkVSIVtx_minOpAng", &trkVSIVtx_minOpAng, &b_trkVSIVtx_minOpAng);
      fChainNominal->SetBranchAddress("trkVSIVtx_num_trks", &trkVSIVtx_num_trks, &b_trkVSIVtx_num_trks);
      fChainNominal->SetBranchAddress("trkVSIVtx_dCloseVrt", &trkVSIVtx_dCloseVrt, &b_trkVSIVtx_dCloseVrt);
      fChainNominal->SetBranchAddress("trknIBLHits", &trknIBLHits, &b_trknIBLHits);
      fChainNominal->SetBranchAddress("trknExpIBLHits", &trknExpIBLHits, &b_trknExpIBLHits);
      fChainNominal->SetBranchAddress("trknExpBLayerHits", &trknExpBLayerHits, &b_trknExpBLayerHits);
      fChainNominal->SetBranchAddress("trknBLayerHits", &trknBLayerHits, &b_trknBLayerHits);
      fChainNominal->SetBranchAddress("trknPixHits", &trknPixHits, &b_trknPixHits);
      fChainNominal->SetBranchAddress("trknPixLayers", &trknPixLayers, &b_trknPixLayers);
      fChainNominal->SetBranchAddress("trknPixDeadSensors", &trknPixDeadSensors, &b_trknPixDeadSensors);
      fChainNominal->SetBranchAddress("trknPixHoles", &trknPixHoles, &b_trknPixHoles);
      fChainNominal->SetBranchAddress("trknPixSharedHits", &trknPixSharedHits, &b_trknPixSharedHits);
      fChainNominal->SetBranchAddress("trknPixOutliers", &trknPixOutliers, &b_trknPixOutliers);
      fChainNominal->SetBranchAddress("trknSCTHits", &trknSCTHits, &b_trknSCTHits);
      fChainNominal->SetBranchAddress("trknSCTDeadSensors", &trknSCTDeadSensors, &b_trknSCTDeadSensors);
      fChainNominal->SetBranchAddress("trknSCTHoles", &trknSCTHoles, &b_trknSCTHoles);
      fChainNominal->SetBranchAddress("trknSCTSharedHits", &trknSCTSharedHits, &b_trknSCTSharedHits);
      fChainNominal->SetBranchAddress("trknSCTOutliers", &trknSCTOutliers, &b_trknSCTOutliers);
      fChainNominal->SetBranchAddress("trkPtcone20", &trkPtcone20, &b_trkPtcone20);
      fChainNominal->SetBranchAddress("trkPtcone30", &trkPtcone30, &b_trkPtcone30);
      fChainNominal->SetBranchAddress("trkPtcone40", &trkPtcone40, &b_trkPtcone40);
      fChainNominal->SetBranchAddress("trkNonBaseAssocPtcone20", &trkNonBaseAssocPtcone20, &b_trkNonBaseAssocPtcone20);
      fChainNominal->SetBranchAddress("trkNonBaseAssocPtcone30", &trkNonBaseAssocPtcone30, &b_trkNonBaseAssocPtcone30);
      fChainNominal->SetBranchAddress("trkNonBaseAssocPtcone40", &trkNonBaseAssocPtcone40, &b_trkNonBaseAssocPtcone40);
      fChainNominal->SetBranchAddress("trkNonBasePhotonConvPtcone20", &trkNonBasePhotonConvPtcone20, &b_trkNonBasePhotonConvPtcone20);
      fChainNominal->SetBranchAddress("trkNonBasePhotonConvPtcone30", &trkNonBasePhotonConvPtcone30, &b_trkNonBasePhotonConvPtcone30);
      fChainNominal->SetBranchAddress("trkNonBasePhotonConvPtcone40", &trkNonBasePhotonConvPtcone40, &b_trkNonBasePhotonConvPtcone40);
      fChainNominal->SetBranchAddress("trkNonSignalAssocPtcone20", &trkNonSignalAssocPtcone20, &b_trkNonSignalAssocPtcone20);
      fChainNominal->SetBranchAddress("trkNonSignalAssocPtcone30", &trkNonSignalAssocPtcone30, &b_trkNonSignalAssocPtcone30);
      fChainNominal->SetBranchAddress("trkNonSignalAssocPtcone40", &trkNonSignalAssocPtcone40, &b_trkNonSignalAssocPtcone40);
      fChainNominal->SetBranchAddress("trkNonSignalPhotonConvPtcone20", &trkNonSignalPhotonConvPtcone20, &b_trkNonSignalPhotonConvPtcone20);
      fChainNominal->SetBranchAddress("trkNonSignalPhotonConvPtcone30", &trkNonSignalPhotonConvPtcone30, &b_trkNonSignalPhotonConvPtcone30);
      fChainNominal->SetBranchAddress("trkNonSignalPhotonConvPtcone40", &trkNonSignalPhotonConvPtcone40, &b_trkNonSignalPhotonConvPtcone40);
      fChainNominal->SetBranchAddress("trkBaseLepPhotonMinDR", &trkBaseLepPhotonMinDR, &b_trkBaseLepPhotonMinDR);
      fChainNominal->SetBranchAddress("trkSignalLepPhotonMinDR", &trkSignalLepPhotonMinDR, &b_trkSignalLepPhotonMinDR);
      fChainNominal->SetBranchAddress("minDPhi_met_allJets20", &minDPhi_met_allJets20, &b_minDPhi_met_allJets20);
      fChainNominal->SetBranchAddress("minDPhi_met_allJets30", &minDPhi_met_allJets30, &b_minDPhi_met_allJets30);
      fChainNominal->SetBranchAddress("hasZCand", &hasZCand, &b_hasZCand);
      fChainNominal->SetBranchAddress("mll", &mll, &b_mll);
      fChainNominal->SetBranchAddress("Photon", &Photon, &b_Photon);
      fChainNominal->SetBranchAddress("Photon_met_Et", &Photon_met_Et, &b_Photon_met_Et);
      fChainNominal->SetBranchAddress("Photon_met_Phi", &Photon_met_Phi, &b_Photon_met_Phi);
      fChainNominal->SetBranchAddress("Photon_minDPhi_met_allJets20", &Photon_minDPhi_met_allJets20, &b_Photon_minDPhi_met_allJets20);
      fChainNominal->SetBranchAddress("Photon_minDPhi_met_allJets30", &Photon_minDPhi_met_allJets30, &b_Photon_minDPhi_met_allJets30);
      fChainNominal->SetBranchAddress("Photon_conversionType", &Photon_conversionType, &b_Photon_conversionType);
      fChainNominal->SetBranchAddress("Photon_conversionRadius", &Photon_conversionRadius, &b_Photon_conversionRadius);
      fChainNominal->SetBranchAddress("DecayProcess", &DecayProcess, &b_DecayProcess);
      fChainNominal->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
      fChainNominal->SetBranchAddress("leptonWeight", &leptonWeight, &b_leptonWeight);
      fChainNominal->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
      fChainNominal->SetBranchAddress("genWeight", &genWeight, &b_genWeight);
      fChainNominal->SetBranchAddress("bTagWeight", &bTagWeight, &b_bTagWeight);
      fChainNominal->SetBranchAddress("trigWeight", &trigWeight, &b_trigWeight);
      fChainNominal->SetBranchAddress("jvtWeight", &jvtWeight, &b_jvtWeight);
      fChainNominal->SetBranchAddress("photonWeight", &photonWeight, &b_photonWeight);
      fChainNominal->SetBranchAddress("genWeightUp", &genWeightUp, &b_genWeightUp);
      fChainNominal->SetBranchAddress("genWeightDown", &genWeightDown, &b_genWeightDown);
      fChainNominal->SetBranchAddress("x1", &x1, &b_x1);
      fChainNominal->SetBranchAddress("x2", &x2, &b_x2);
      fChainNominal->SetBranchAddress("pdf1", &pdf1, &b_pdf1);
      fChainNominal->SetBranchAddress("pdf2", &pdf2, &b_pdf2);
      fChainNominal->SetBranchAddress("scalePDF", &scalePDF, &b_scalePDF);
      fChainNominal->SetBranchAddress("id1", &id1, &b_id1);
      fChainNominal->SetBranchAddress("id2", &id2, &b_id2);
      fChainNominal->SetBranchAddress("PRWHash", &PRWHash, &b_PRWHash);
      fChainNominal->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
      fChainNominal->SetBranchAddress("xsec", &xsec, &b_xsec);
      fChainNominal->SetBranchAddress("GenHt", &GenHt, &b_GenHt);
      fChainNominal->SetBranchAddress("GenMET", &GenMET, &b_GenMET);
      fChainNominal->SetBranchAddress("DatasetNumber", &DatasetNumber, &b_DatasetNumber);
      fChainNominal->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
      fChainNominal->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
      fChainNominal->SetBranchAddress("FS", &FS, &b_FS);
      fChainNominal->SetBranchAddress("LHE3Weights", &LHE3Weights, &b_LHE3Weights);
      fChainNominal->SetBranchAddress("LHE3WeightNames", &LHE3WeightNames, &b_LHE3WeightNames);

   }
   else{
  
      // To be implemented when running on systematics
  
   }

   // Hack to handle name exceptions
   tree_name_strs.clear();
   if (tree_name.find("diboson") != std::string::npos){
     boost::split(tree_name_strs, tree_name, boost::is_any_of("_"));
     tree_name = tree_name_strs[0];
     for (Int_t i = 3; i < tree_name_strs.size(); i++) tree_name += "_" + tree_name_strs[i];
   }
   else if (tree_name.find("MGPy8EG_A14N23LO_HH") != std::string::npos){
     boost::split(tree_name_strs, tree_name, boost::is_any_of("_"));
     tree_name = "Higgsino";
     for (Int_t i = 7; i < tree_name_strs.size(); i++) tree_name += "_" + tree_name_strs[i];
   } 
   else if (tree_name.find("Znn") != std::string::npos || tree_name.find("Zee") != std::string::npos || tree_name.find("Zmm") != std::string::npos || tree_name.find("Ztt") != std::string::npos){
     boost::split(tree_name_strs, tree_name, boost::is_any_of("_"));
     tree_name = "Zjets";
     for (Int_t i = 1; i < tree_name_strs.size(); i++) tree_name += "_" + tree_name_strs[i];
   }
   else if (tree_name.find("Wtn") != std::string::npos || tree_name.find("Wen") != std::string::npos || tree_name.find("Wmn") != std::string::npos){
     boost::split(tree_name_strs, tree_name, boost::is_any_of("_"));
     tree_name = "Wlnjets";
     for (Int_t i = 1; i < tree_name_strs.size(); i++) tree_name += "_" + tree_name_strs[i];
   }

   // Create output tree with name defined above
   if (tree_name.find("data1") != std::string::npos || tree_name.find("data2") != std::string::npos) 
      output_tree = new TTree("data", "data");
   else if (tree_name.find("diboson") != std::string::npos || tree_name.find("Higgsino") != std::string::npos || tree_name.find("Zjets") != std::string::npos || tree_name.find("Wlnjets") != std::string::npos) 
      output_tree = new TTree(tree_name.c_str(), tree_name.c_str());
   else 
      output_tree = new TTree(tree->GetName(), tree->GetName());

   // Output tree branches
   //output_tree->Branch("mu", &MU, "mu/F");                            
   output_tree->Branch("EventNumber", &EVENTNumber, "EventNumber/I"); 
   output_tree->Branch("RunNumber", &RUNnumber, "RunNumber/I"); 
   //output_tree->Branch("dsid", &DSID, "dsid/I");                        
   output_tree->Branch("genWeight", &GenWeight, "genWeight/F");                    
   output_tree->Branch("eventWeight", &EventWeight, "eventWeight/F");                  
   output_tree->Branch("leptonWeight", &LeptonWeight, "leptonWeight/F");                 
   output_tree->Branch("photonWeight", &PhotonWeight, "photonWeight/F");                 
   output_tree->Branch("JVTWeight", &JVTWeight, "JVTWeight/F");                 
   output_tree->Branch("pileupWeight", &PileupWeight, "pileupWeight/F");                 
   output_tree->Branch("trigWeight", &TrigWeight, "trigWeight/F");                   
   output_tree->Branch("trigWeight_singleElectronTrig", &TrigWeight_Ele, "trigWeight_singleElectronTrig/F");                
   output_tree->Branch("trigWeight_singleMuonTrig", &TrigWeight_Mu, "trigWeight_singleMuonTrig/F");               
   output_tree->Branch("trigWeight_singlePhotonTrig", &TrigWeight_Gamma, "trigWeight_singlePhotonTrig/F");             
   output_tree->Branch("trigMatch_metTrig", &TrigMatch_met, "trigMatch_metTrig/I");                
   output_tree->Branch("trigMatch_singleElectronTrig", &TrigMatch_Ele, "trigMatch_singleElectronTrig/I");                
   output_tree->Branch("trigMatch_singleMuonTrig", &TrigMatch_Mu, "trigMatch_singleMuonTrig/I");                 
   output_tree->Branch("trigMatch_singlePhotonTrig", &TrigMatch_Gamma, "trigMatch_singlePhotonTrig/I"); 
   output_tree->Branch("nEle_base", &nEleBase, "nEle_base/I");                     
   output_tree->Branch("nEle_signal", &nEleSig, "nEle_signal/I");                      
   output_tree->Branch("nMu_base", &nMuBase, "nMu_base/I");                      
   output_tree->Branch("nMu_signal", &nMuSig, "nMu_signal/I");                       
   output_tree->Branch("nPhoton_base", &nPhotonBase, "nPhoton_base/I");                  
   output_tree->Branch("nPhoton_signal", &nPhotonSignal, "nPhoton_signal/I");                
   //output_tree->Branch("hasZCand", &HasZCand, "hasZCand/I");                     
   //output_tree->Branch("mll", &Mll, "mll/F");  
   //output_tree->Branch("mT", &mT, "mT/F");                        
   //output_tree->Branch("lep1Flavor", &Lep1Flavor, "lep1Flavor/I");                   
   //output_tree->Branch("lep1Q", &Lep1Q, "lep1Q/I");                        
   //output_tree->Branch("lep1Pt", &Lep1Pt, "lep1Pt/F");                       
   //output_tree->Branch("lep1Eta", &Lep1Eta, "lep1Eta/F");                      
   //output_tree->Branch("lep1Phi", &Lep1Phi, "lep1Phi/F");                      
   //output_tree->Branch("lep2Flavor", &Lep2Flavor, "lep2Flavor/I");                   
   //output_tree->Branch("lep2Q", &Lep2Q, "lep2Q/I");                       
   //output_tree->Branch("lep2Pt", &Lep2Pt, "lep2Pt/F");                       
   //output_tree->Branch("lep2Eta", &Lep2Eta, "lep2Eta/F");                      
   //output_tree->Branch("lep2Phi", &Lep2Phi, "lep2Phi/F");                      
   output_tree->Branch("nJet", &nJet, "nJet/I");                         
   output_tree->Branch("leadJetPt", &LeadJetPt, "leadJetPt/F");                    
   output_tree->Branch("leadJetEta", &LeadJetEta, "leadJetEta/F");                   
   output_tree->Branch("leadJetPhi", &LeadJetPhi, "leadJetPhi/F");                   
   output_tree->Branch("leadJetM", &LeadJetM, "leadJetM/F");                   
   output_tree->Branch("MET", &MET, "MET/F");                          
   output_tree->Branch("MET_invis_electrons", &MET_electrons, "MET_invis_electrons/F");                
   output_tree->Branch("MET_invis_muons", &MET_muons, "MET_invis_muons/F");                    
   output_tree->Branch("MET_invis_photons", &MET_photons, "MET_invis_photons/F");                  
   output_tree->Branch("MET_invis_leptons", &MET_leptons, "MET_invis_leptons/F");                  
   output_tree->Branch("MET_Phi", &MET_Phi, "MET_Phi/F");                      
   output_tree->Branch("MET_invis_electrons_Phi", &MET_electrons_Phi, "MET_invis_electrons_Phi/F");            
   output_tree->Branch("MET_invis_muons_Phi", &MET_muons_Phi, "MET_invis_muons_Phi/F");                
   output_tree->Branch("MET_invis_photons_Phi", &MET_photons_Phi, "MET_invis_photons_Phi/F");              
   output_tree->Branch("MET_invis_leptons_Phi", &MET_leptons_Phi, "MET_invis_leptons_Phi/F");              
   output_tree->Branch("MET_Signif", &MET_Signif, "MET_Signif/F");                   
   //output_tree->Branch("MET_invis_electrons_Signif", &MET_electrons_Signif, "MET_invis_electrons_Signif/F");         
   //output_tree->Branch("MET_invis_muons_Signif", &MET_muons_Signif, "MET_invis_muons_Signif/F");             
   //output_tree->Branch("MET_invis_photons_Signif", &MET_photons_Signif, "MET_invis_photons_Signif/F");           
   //output_tree->Branch("MET_invis_leptons_Signif", &MET_leptons_Signif, "MET_invis_leptons_Signif/F");           
   output_tree->Branch("MET_minDPhiJetMET", &MET_dphi, "MET_minDPhiJetMET/F");                     
   //output_tree->Branch("MET_invis_electrons_minDPhiJetMET", &MET_electrons_dphi, "MET_invis_electrons_minDPhiJetMET/F");           
   //output_tree->Branch("MET_invis_muons_minDPhiJetMET", &MET_muons_dphi, "MET_invis_muons_minDPhiJetMET/F");               
   //output_tree->Branch("MET_invis_photons_minDPhiJetMET", &MET_photons_dphi, "MET_invis_photons_minDPhiJetMET/F");             
   //output_tree->Branch("MET_invis_leptons_minDPhiJetMET", &MET_leptons_dphi, "MET_invis_leptons_minDPhiJetMET/F");             
   output_tree->Branch("nTrack", &nTrack, "nTrack/I");                      
   output_tree->Branch("trackPt", &TrackPt, "trackPt/F");                      
   output_tree->Branch("trackEta", &TrackEta, "trackEta/F");                     
   output_tree->Branch("trackPhi", &TrackPhi, "trackPhi/F");                     
   output_tree->Branch("trackZ0", &TrackZ0, "trackZ0/F");                      
   output_tree->Branch("trackD0", &TrackD0, "trackD0/F");                      
   output_tree->Branch("trackD0Sig", &TrackD0Sig, "trackD0Sig/F");                   
   output_tree->Branch("trackZ0SinTheta", &TrackZ0SinTheta, "trackZ0SinTheta/F");              
   output_tree->Branch("trackQ", &TrackCharge, "trackQ/I");                 
   output_tree->Branch("trackMt", &TrackMt, "trackMt/F");
   output_tree->Branch("trackFitQuality", &TrackFitQuality, "trackFitQuality/F");              
   output_tree->Branch("trackOrigin", &TrackOrigin, "trackOrigin/I");                  
   output_tree->Branch("trackType", &TrackType, "trackType/I");                    
   output_tree->Branch("trackLabel", &TrackLabel, "trackLabel/I");                   
   //output_tree->Branch("trackPdgId", &TrackPdgId, "trackPdgId/I");                   
   //output_tree->Branch("trackParPdgId", &TrackParPdgId, "trackParPdgId/I");                
   output_tree->Branch("trackNIBLHits", &TrackNIBLHits, "trackNIBLHits/I");                
   output_tree->Branch("trackPixHits", &TrackPixHits, "trackPixHits/I");    
   output_tree->Branch("trackPixeldEdx", &TrackPixeldEdx, "trackPixeldEdx/F");    
   output_tree->Branch("trackTRTdEdx", &TrackTRTdEdx, "trackTRTdEdx/F");    
   output_tree->Branch("trackIsBaseMuon", &TrackIsBaseMuon, "trackIsBaseMuon/I");              
   output_tree->Branch("trackIsBaseElectron", &TrackIsBaseElectron, "trackIsBaseElectron/I");          
   output_tree->Branch("trackIsBasePhoton", &TrackIsBasePhoton, "trackIsBasePhoton/I");            
   //output_tree->Branch("trackIsSignalMuon", &TrackIsSignalMuon, "trackIsSignalMuon/I");            
   //output_tree->Branch("trackIsSignalElectron", &TrackIsSignalElectron, "trackIsSignalElectron/I");        
   //output_tree->Branch("trackIsSignalPhoton", &TrackIsSignalPhoton, "trackIsSignalPhoton/I");          
   output_tree->Branch("trackIsSecTrk", &TrackIsSecTrk, "trackIsSecTrk/I");                
   //output_tree->Branch("trackMatchedToVSI", &TrackMatchedToVSI, "trackMatchedToVSI/I");            
   //output_tree->Branch("trkNonBaseAssocPtcone20", &TrkNonBaseAssocPtcone20, "trkNonBaseAssocPtcone20/F");      
   output_tree->Branch("trkNonBaseAssocPtcone30", &TrkNonBaseAssocPtcone30, "trkNonBaseAssocPtcone30/F");      
   output_tree->Branch("trkNonBaseAssocPtcone40", &TrkNonBaseAssocPtcone40, "trkNonBaseAssocPtcone40/F");      
   //output_tree->Branch("trkNonBasePhotonConvPtcone20", &TrkNonBasePhotonConvPtcone20, "trkNonBasePhotonConvPtcone20/F"); 
   output_tree->Branch("trkNonBasePhotonConvPtcone30", &TrkNonBasePhotonConvPtcone30, "trkNonBasePhotonConvPtcone30/F"); 
   output_tree->Branch("trkNonBasePhotonConvPtcone40", &TrkNonBasePhotonConvPtcone40, "trkNonBasePhotonConvPtcone40/F"); 
   //output_tree->Branch("trkNonSignalAssocPtcone20", &TrkNonSignalAssocPtcone20, "trkNonSignalAssocPtcone20/F");    
   output_tree->Branch("trkNonSignalAssocPtcone30", &TrkNonSignalAssocPtcone30, "trkNonSignalAssocPtcone30/F");    
   output_tree->Branch("trkNonSignalAssocPtcone40", &TrkNonSignalAssocPtcone40, "trkNonSignalAssocPtcone40/F");    
   //output_tree->Branch("trkNonSignalPhotonConvPtcone20", &TrkNonSignalPhotonConvPtcone20, "trkNonSignalPhotonConvPtcone20/F");
   output_tree->Branch("trkNonSignalPhotonConvPtcone30", &TrkNonSignalPhotonConvPtcone30, "trkNonSignalPhotonConvPtcone30/F");
   output_tree->Branch("trkNonSignalPhotonConvPtcone40", &TrkNonSignalPhotonConvPtcone40, "trkNonSignalPhotonConvPtcone40/F");

}

Bool_t analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;

}

void analysis::Show(TTree* tree, Long64_t entry)
{

   // Print contents of entry.
   // If entry is not specified, print current entry
   if (!tree) return;
   tree->Show(entry);

}

Int_t analysis::Cut(Long64_t entry)
{

   // This function may be called from Loop.
   // returns  1 if entry is accepted.
   // returns -1 otherwise.
   return 1;

}

Float_t analysis::GetgenWeight() {

   // When running on data or when genWeight
   // has already been computed, just return
   // genWeight
   if (m_cbkSOWMap.empty()) 
      return genWeight;

   std::tuple<Int_t, Int_t> tuple = std::make_tuple(DatasetNumber, RunNumber);
   
   // If genWeight needs to be re-computed, then consider also the relative campaign luminosity
   // Recommendations here: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LuminosityForPhysics)
   // TO DO:
   // - keep luminosity updated, currently using:
   //    - 140.06894 fb^{-1} as total lumi for mc20
   //    - 51.8389 fb^{-1} as total lumi for mc23
   if( std::get<1>(tuple) == 284500){      // mc20a
      return genWeight * 0.26163 / m_cbkSOWMap[tuple];
   }
   else if(std::get<1>(tuple) == 300000){ // mc20c/d
      return genWeight * 0.31863 / m_cbkSOWMap[tuple];  
   }
   else if(std::get<1>(tuple) == 310000){ // mc20e
      return genWeight * 0.41973 / m_cbkSOWMap[tuple];  
   }
   else if(std::get<1>(tuple) == 410000){ // mc23a
      return genWeight * 0.50293 / m_cbkSOWMap[tuple]; 
   }
   else if(std::get<1>(tuple) == 450000){ // mc23c/d
      return genWeight * 0.49707 / m_cbkSOWMap[tuple]; 
   }
   else if(std::get<1>(tuple) == 470000){ // mc23e
      return genWeight;
   }
   else {
      std::cout << "Unknown RunNumber requested when computing genWeight...aborting" << std::endl;
      abort();
   }

}

void analysis::GetSumOfWeights(TTree *CBKTree)
{

   if (!CBKTree) {
      std::cout << "You want to retrieve sumOfWeights but no CBK Tree was specified...aborting" << std::endl;
      abort();
   }

   // Get all needed branches
   UInt_t run_number, dsid;
   Float_t SOW;
   CBKTree->SetBranchAddress("RunNumber", &run_number);
   CBKTree->SetBranchAddress("DatasetNumber", &dsid);
   CBKTree->SetBranchAddress("AllExecutedEvents_sumOfEventWeights", &SOW);
   
   // Loop over CBK tree and sum up the sumws
   for (Int_t i = 0; i < CBKTree->GetEntries(); i++) {

      int getEntry = CBKTree->GetEntry(i);

      if(getEntry == -1) {
        std::cout << "I/O error occurred when retrieving CBK Tree entries...aborting" << std::endl;
        abort();
      }
      else if(getEntry == 0) { // Invalid entry...no worries, just skip it
        continue;
      }

      // Check if <DSID, RunNUmber> tuple already exist. If yes then 
      // update its sumOfWeights, otherwise insert the tuple in the map
      std::tuple<UInt_t, UInt_t> tuple = std::make_tuple(dsid, run_number);
      if (m_cbkSOWMap.find(tuple) != m_cbkSOWMap.end()) {
         m_cbkSOWMap[tuple] += SOW;
      }
      else{
         m_cbkSOWMap.emplace(tuple, SOW);
      }

   }

}

void analysis::SaveLHE3Weights(std::vector<float> *LHE3Weights, std::vector<TString> *LHE3WeightNames, std::string tree_name, Int_t dsid)
{

   // To be implemented when running on systematics

}

void analysis::FillHistogram(TH1D *h, Double_t entry, Double_t weight)
{

  Double_t max = h->GetXaxis()->GetXmax();
  Double_t min = h->GetXaxis()->GetXmin();

  if(entry > max) entry = max - 1E-8;
  if(entry < min) entry = min + 1E-8;

  h->Fill(entry, weight);

  return;

}

#endif // #ifdef analysis_cxx
