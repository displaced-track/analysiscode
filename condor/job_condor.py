import os, sys

memory = "4096"
CPUs   = "1"

samples_list = ["sample_name"]

for filename in samples_list:

  print ("Creating submission files for " + filename)
  
  workPath = "/path/to/this/folder/"
  
  template_condor = """executable     = {}.sh
arguments      = {}
output         = {}.out
error          = {}.err
log            = {}.log
+JobFlavour    = "tomorrow"
Requirements = ClusterName == "proof-pool"
request_memory = {}
request_cpus = {}
queue"""
      
  setup = """export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup root """
  
  command = "python run_analysis.py -i /path/to/input/ntuples/ -n sample_name -o /path/to/output/ntuples/ --MaxEvents -1"

  job_file_name = "{}.sh".format(filename)
  job_file = open(workPath + "/" + job_file_name, "w")
  job_file.write("#!/bin/bash\n")
  job_file.write(setup + "\n")
  job_file.write("cd ../\n")
  job_file.write(command.format(filename, filename) + "\n")
  
  output = "{}.out".format(filename)
  error = "{}.err".format(filename)
  log = "{}.log".format(filename)
  job_file.close()

  submit_file_name = "{}.sub".format(filename)
  submit_file = open(workPath + "/" + submit_file_name, "w")
  tc = template_condor.format(filename, filename, filename, filename, filename, memory, CPUs)
  submit_file.write(tc + "\n")
