import ROOT
from ROOT import *
import os
import argparse
import re

ROOT.gROOT.LoadMacro("analysis.C")

parser = argparse.ArgumentParser(description = 'Flags needed to steer the analysis code')

parser.add_argument('-i', '--InputPath',       default = './',  action = 'store',      type = str,  help = 'Path to input directory')
parser.add_argument('-o', '--OutputDirectory', default = './',  action = 'store',      type = str,  help = 'Path to output directory')
parser.add_argument('-n', '--name',            default = '',    action = 'store',      type = str,  help = 'Use this flag if you want to process only one specific sample from input path')
parser.add_argument(      '--MaxEvents',       default = '-1',  action = 'store',      type = int,  help = 'Maximum number of events to be processed')
parser.add_argument(      '--getSumOfWeights', default = False, action = 'store_true',              help = 'Get sumOfWeights from CBK tree')

args            = parser.parse_args()
InputPath       = args.InputPath 
OutputDirectory = args.OutputDirectory 
name            = args.name
MaxEvents       = args.MaxEvents
getSumOfWeights = args.getSumOfWeights

for sample in os.listdir(InputPath):

	# If used, skip all samples whose name is not
	# equal to the one provided by the user
	if name not in sample: continue

	# Make sure we are reading trees from .root file
	if ".root" not in sample: continue

	# Setup analysis class
	ana = ROOT.analysis()

	print("Reading sample " + sample)
	ROOTFile = TFile(InputPath + "/" + sample)

	# Loop over trees in ROOTFile
	for key in ROOTFile.GetListOfKeys():

		# Check we are retrieving trees only
		if key.GetClassName() != "TTree": continue    
		chain = ROOTFile.Get(key.GetName())

		# Check we are working on nominal and syst 
		# trees only
		if "MetaTree" in chain.GetName(): continue
		
		if "CutBookkeepers" in chain.GetName() and "data" not in sample and getSumOfWeights == True:
			
			# If user specifies it (i.e. no run_merge done after producing the 
	  	# ntuples with SusySkimHiggsino), get the sumOfWeights to manually 
	  	# re-compute the correct values for genWeight
			print("Getting sumOfWeights from CBK as requested...")
			ana.GetSumOfWeights(chain)
		
		elif "CutBookkeepers" not in chain.GetName():
			
			# Do the analysis in the other case and save output trees
			print("Reading " + chain.GetName() + " with " + str(chain.GetEntries()) + " entries")
			OutFileName = OutputDirectory + sample
			ana.LoopAndWrite(chain, OutFileName, MaxEvents)	